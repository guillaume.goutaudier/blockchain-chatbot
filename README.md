# Introduction

The objective of this tutorial is to demonstrate how to build a simple Digital Assistant (aka chatbot) that can query Ethereum addresses.

The architecture that we propose to build is the following:
![architecture.png](pics/architecture.png)

Note that in order to run this tutorial, an Oracle Cloud account is required. A 30-day free trial is available on https://www.oracle.com/cloud/

# Deployment of the Node.js backend
### Create a Virtual Machine
Deploy a Standard2.1 virtual machine (the smallest shape is sufficient) with OS Image "Canonical Ubuntu 20.04". When done, run this script to setup the chatbot node.js environment (this will take up to 5min, you can directly look at the script to see the details of the commands):
```
bash <(curl -s https://gitlab.com/guillaume.goutaudier/blockchain-chatbot/-/raw/master/init-script.sh)
```

### Start Backend Service
```
npm start
```

Connect to `http://<your_server_ip>/components` and check that you get the JSON definition of your components.

# Deployment of the Digital Assistant

### Upload of the BlockchainQuery Skill
- From your Oracle Cloud console, go to `Digital Assistant` and create a Digital Assitant Instance. 
- After the Digital Assistance instance is created, connect to it. 
- Click on the Hamburger on the top left of the screen, go to Skills and click on `Import Skill`.
- Select the `BlockchainQuery.zip` file. A small warning indicates an error loading the components. This is what we are going to fix now.
- Open the BlockchainQuery skill and go to the Components section.
- In the Metadata URL, replace the IP in the Metadata URL with the IP of the previously created VM. Put a random password (any password will be accepted). If status does not automatically change to "Ready", click `Reload`. 

# Training and Test of the Chatbot
- Click on the `Train` button on the top right of the screen. This will train the chatbot to recognize the various user intents based on the provided samples sentences. You can kill the default options.

- Click on the `Validate` button on the top right of the screen. This will validte that the conversation script is OK.

- Click on the `Play` button and have fun:
![screenshot.png](pics/screenshot.png)


# References
https://docs.oracle.com/en/cloud/paas/digital-assistant/use-chatbot/

