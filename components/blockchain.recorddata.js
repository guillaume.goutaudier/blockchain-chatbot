'use strict';

var request = require('request');

module.exports = {
  metadata: () => ({
    name: 'blockchain.recorddata',
    properties: {
      data: { required: true, type: 'string' },
    },
    supportedActions: []
  }),
  invoke: (conversation, done) => {
    // parse variables
    const data = conversation.properties().data;
    const url = 'http://api-test.swissledger.io:5000/dev/v3/document/uploadHash';

    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

   var dataString = `{"hash": "${data}", "blockchain_connector": "1", "storage_connector": "0", "use_tx": "yes"}`;

   var options = {
     url: url,
     method: 'POST',
     headers: headers,
     body: dataString
    };

    // send_data_to_blockchain
    request(options, (err, res, body) => {
      var message = "ERROR recording data";
      if (!err && res.statusCode == 200) {
	  var body_json = JSON.parse(body);
          var tx = body_json["transaction_hash"];
	  message = `Successfully recorded your message on Goerli testnet.\nTransaction hash: ${tx}.\nYou can check the transaction on https://goerli.etherscan.io/tx/${tx}`;
          //conversation.variable("balance", balance);
      } 
      conversation
        .reply(message)
        .keepTurn(true)
        .transition();

      done();
    });

  }
};
