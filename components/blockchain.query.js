'use strict';

var request = require('request');

module.exports = {
  metadata: () => ({
    name: 'blockchain.query',
    properties: {
      currency: { required: true, type: 'string' },
      address: { required: true, type: 'string' },
    },
    supportedActions: []
  }),
  invoke: (conversation, done) => {
    // parse variables
    const currency = conversation.properties().currency;
    const address = conversation.properties().address;
    const api_key = 'D4VMFNURNQJKHHD5ZQEK7RYX24MVUQ8F2H';
    const dai_contract_address = '0x6b175474e89094c44da98b954eedeac495271d0f';
    var balance = conversation.properties().balance;
    // craft Etherscan URL
    var url = "";
    if ( currency == 'ETH') {
      url = `http://api.etherscan.io/api?module=account&action=balance&address=${address}&tag=latest&apikey=${api_key}`;
    }
    if ( currency == 'DAI') {
      url = `http://api.etherscan.io/api?module=account&action=tokenbalance&contractaddress=${dai_contract_address}&address=${address}&tag=latest&apikey=${api_key}`;

    }
    // reply
    request(url, (err, res, body) => {
      var message = "ERROR retrieving balance";
      if (!err && res.statusCode == 200) {
          var body_json = JSON.parse(body);
          balance = body_json["result"];
          conversation.variable("balance", balance);
      } 
      conversation
        .keepTurn(true)
        .transition();

      done();
    });

  }
};
