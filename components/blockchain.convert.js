'use strict';

var request = require('request');
var web3 = require('web3');

module.exports = {
  metadata: () => ({
    name: 'blockchain.convert',
    properties: {
      currency: { required: true, type: 'string' },
      balance: { required: true, type: 'string' },
    },
    supportedActions: []
  }),
  invoke: (conversation, done) => {
    // parse variables
    const currency = conversation.properties().currency;
    const balance = conversation.properties().balance;
    // craft Etherscan URL
    var converted_balance = "";
    if ( currency == 'ETH') {
      converted_balance = web3.utils.fromWei(balance, 'ether');
    }
    if ( currency == 'DAI') {
      converted_balance = web3.utils.fromWei(balance, 'ether');
    }
    // reply
    var message = `Your balance is ${converted_balance} ${currency}`;
    conversation
      .reply(message)
      .transition();

    done();

  }
};
